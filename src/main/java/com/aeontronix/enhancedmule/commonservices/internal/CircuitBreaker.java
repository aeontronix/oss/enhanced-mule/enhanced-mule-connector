package com.aeontronix.enhancedmule.commonservices.internal;

import com.aeontronix.enhancedmule.commonservices.api.cluster.ClusterService;
import com.aeontronix.enhancedmule.commonservices.api.CircuitBreakerState;
import com.hazelcast.crdt.pncounter.PNCounter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;

import static org.slf4j.LoggerFactory.getLogger;

@SuppressWarnings("UnusedReturnValue")
public class CircuitBreaker {
    private static final Logger logger = getLogger(CircuitBreaker.class);
    public static final String CBREAKER_ERR = "cbreaker_err_";
    public static final String CBREAKER_OP = "cbreaker_op_";
    private final ClusterService instance;
    private String circuitBreakerId;
    private String durationString;
    private BigDecimal errorTreshold;
    private String closedDuration;
    private CircuitBreakerState state;

    public CircuitBreaker(String circuitBreakerId, String durationString, BigDecimal errorTreshold, String closedDuration) {
        this.circuitBreakerId = circuitBreakerId;
        this.durationString = durationString;
        this.errorTreshold = errorTreshold;
        this.closedDuration = closedDuration;
        instance = ClusterService.getInstance();
        final CircuitBreakerPersistedState circuitBreakerPersistedState = instance.getCircuitBreakerState().get(circuitBreakerId);
        if (circuitBreakerPersistedState != null) {
            state = circuitBreakerPersistedState.getState();
        } else {
            state = CircuitBreakerState.OPEN;
        }
        logger.debug("Circuit breaker {} loaded state: {}", circuitBreakerId, state);
    }

    public CircuitBreakerState getState() {
        return state;
    }

    public synchronized long incErrors() {
        try {
            return doIncErrors();
        } catch (Throwable e) {
            logger.error("An error occurred while increasing errors");
            return 0;
        }
    }

    public synchronized void closeCircuit() {
        try {
            doCloseCircuit(instance, circuitBreakerId, closedDuration);
        } catch (Throwable e) {
            logger.error("An error occurred while closing circuit");
        }
    }

    private long doIncErrors() {
        doIncOperations();
        final long count = getErrorCounter().incrementAndGet();
        logger.debug("Incremented error counter for circuit breaker {}. counter is now {}", circuitBreakerId, count);
        return count;
    }

    public synchronized long incOperations() {
        try {
            return doIncOperations();
        } catch (Throwable e) {
            logger.error("An error occurred while increasing errors");
            return 0;
        }
    }

    private long doIncOperations() {
        final long count = getOperationsCounter().incrementAndGet();
        logger.debug("Incremented operation counter for circuit breaker {}. counter is now {}", circuitBreakerId, count);
        return count;
    }

    @NotNull
    private PNCounter getErrorCounter() {
        return getErrorCounter(instance, circuitBreakerId);
    }

    @NotNull
    private PNCounter getOperationsCounter() {
        return getOperationsCounter(instance, circuitBreakerId);
    }

    public synchronized void handleErrorInOpenState() {
        try {
            doIncErrors();
            changeCircuitBreakerState(instance, circuitBreakerId, CircuitBreakerState.OPEN_WITH_ERROR);
            final BigDecimal thresholdValue = errorTreshold.setScale(2, RoundingMode.HALF_UP)
                    .divide(new BigDecimal(100), RoundingMode.HALF_UP);
            instance.circuitBreakerSchedule(new OpenWithErrorScheduledProcessor(circuitBreakerId,
                    thresholdValue, closedDuration), durationString);
            logger.debug("Updated circuit breaker {} state to OPEN_WITH_ERROR", circuitBreakerId);
            doIncOperations();
            doIncOperations();
        } catch (Throwable e) {
            logger.error("Failed to change circuit breaker state to OPEN_WITH_ERROR: " + e.getMessage(), e);
        }
    }

    public synchronized void openCircuit() {
        try {
            doOpenCircuit(instance, circuitBreakerId);
        } catch (Exception e) {
            logger.error("An error occurred while attempting to open circuit, scheduling retry in 5 minutes");
            instance.circuitBreakerSchedule(this::openCircuit, "PT5M");
        }
    }

    private static class OpenWithErrorScheduledProcessor implements Runnable {
        private String circuitBreakerId;
        private BigDecimal errorThreshold;
        private String closedDuration;

        public OpenWithErrorScheduledProcessor(String circuitBreakerId, BigDecimal errorThreshold, String closedDuration) {
            this.circuitBreakerId = circuitBreakerId;
            this.errorThreshold = errorThreshold;
            this.closedDuration = closedDuration;
        }

        @Override
        public void run() {
            final BigDecimal errorRatio = getErrorRatio(ClusterService.getInstance(), circuitBreakerId);
            logger.debug("Circuit breaker {} reached time milestone, error ratio is {} and threshold {}", circuitBreakerId, errorRatio, errorThreshold);
            final ClusterService clusterService = ClusterService.getInstance();
            if (errorRatio.compareTo(errorThreshold) < 0) {
                logger.debug("Error ratio not reached, switching circuit breaker back to OPEN: {}", circuitBreakerId);
                doOpenCircuit(clusterService, circuitBreakerId);
            } else {
                logger.error("Error ratio {} is higher than threshold {}, closing circuit breaker: {}", errorRatio, errorThreshold, circuitBreakerId);
                doCloseCircuit(clusterService, circuitBreakerId, closedDuration);
            }
        }
    }

    private static class ClosedScheduledProcessor implements Runnable {
        private String circuitBreakerId;

        public ClosedScheduledProcessor(String circuitBreakerId) {
            this.circuitBreakerId = circuitBreakerId;
        }

        @Override
        public void run() {
            final ClusterService clusterService = ClusterService.getInstance();
            logger.debug("Switching closed circuit breaker to half-open");
            changeCircuitBreakerState(clusterService, circuitBreakerId, CircuitBreakerState.HALF_OPEN);
        }
    }


    private static void resetCounter(PNCounter errorCounter) {
        if (errorCounter != null) {
            errorCounter.destroy();
        }
    }

    public static BigDecimal getErrorRatio(ClusterService instance, String circuitBreakerId) {
        final long ops = getOperationsCounter(instance, circuitBreakerId).get();
        final long errors = getErrorCounter(instance, circuitBreakerId).get();
        final BigDecimal ratio = new BigDecimal(errors).setScale(2, RoundingMode.HALF_UP)
                .divide(new BigDecimal(ops), RoundingMode.HALF_UP);
        logger.debug("Found {} operations and {} errors for circuit breaker {}, ratio is {}", ops, errors, circuitBreakerId, ratio);
        return ratio;
    }

    @NotNull
    private static PNCounter getErrorCounter(ClusterService instance, String circuitBreakerId) {
        return instance.getCircuitBreakerErrorCounter().computeIfAbsent(circuitBreakerId,
                s -> instance.getHazelcastInstance().getPNCounter(CBREAKER_ERR + s));
    }

    @NotNull
    private static PNCounter getOperationsCounter(ClusterService instance, String circuitBreakerId) {
        return instance.getCircuitBreakerOperationsCount().computeIfAbsent(circuitBreakerId,
                s -> instance.getHazelcastInstance().getPNCounter(CBREAKER_OP + s));
    }

    private static void doCloseCircuit(ClusterService clusterService, String circuitBreakerId, String closedDuration) {
        changeCircuitBreakerState(clusterService, circuitBreakerId, CircuitBreakerState.CLOSED);
        resetCounters(clusterService, circuitBreakerId);
        clusterService.circuitBreakerSchedule(new ClosedScheduledProcessor(circuitBreakerId), closedDuration);
    }

    private static void doOpenCircuit(ClusterService clusterService, String circuitBreakerId) {
        changeCircuitBreakerState(clusterService, circuitBreakerId, CircuitBreakerState.OPEN);
        resetCounters(clusterService, circuitBreakerId);
    }

    private static void changeCircuitBreakerState(ClusterService clusterService, String circuitBreakerId, CircuitBreakerState state) {
        changeCircuitBreakerState(clusterService, circuitBreakerId, state, null);
    }

    private static void changeCircuitBreakerState(ClusterService clusterService, String circuitBreakerId, CircuitBreakerState state, String duration) {
        logger.debug("Circuit breaker is now {}: {}", state, circuitBreakerId);
        clusterService.getCircuitBreakerState().put(circuitBreakerId, new CircuitBreakerPersistedState(state,
                duration != null ? Duration.parse(duration).toMillis() + System.currentTimeMillis() : null));
    }

    private static void resetCounters(ClusterService clusterService, String circuitBreakerId) {
        resetCounter(clusterService.getCircuitBreakerErrorCounter().remove(circuitBreakerId));
        resetCounter(clusterService.getCircuitBreakerOperationsCount().remove(circuitBreakerId));
    }
}
