package com.aeontronix.enhancedmule.commonservices.internal;

import com.aeontronix.enhancedmule.commonservices.api.cluster.ClusterService;
import org.mule.runtime.api.exception.MuleException;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

/**
 * This class represents an extension configuration, values set in this class are commonly used across multiple
 * operations since they represent something core from the extension.
 */
@Operations(EMCommonServicesOperations.class)
public class EMCommonServicesConfiguration implements Startable, Stoppable {
    @Parameter
    @Optional
    @DisplayName("Cluster members addresses")
    @Summary("Comma separated list of member addresses")
    private String memberAddresses;
    @Parameter
    @Optional(defaultValue = "7001")
    private int port = 7001;

    public String getMemberAddresses() {
        return memberAddresses;
    }

    public void setMemberAddresses(String memberAddresses) {
        this.memberAddresses = memberAddresses;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public void start() throws MuleException {
        ClusterService.launch(this);
    }

    @Override
    public void stop() throws MuleException {
        ClusterService.getInstance().shutdown();
    }
}
