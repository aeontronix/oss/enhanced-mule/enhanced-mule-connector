package com.aeontronix.enhancedmule.commonservices.internal;

import com.aeontronix.enhancedmule.commonservices.api.CircuitBreakerState;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class CircuitBreakerPersistedState implements Externalizable {
    private CircuitBreakerState state;
    private Long deadline;

    public CircuitBreakerPersistedState() {
    }

    public CircuitBreakerPersistedState(CircuitBreakerState state, Long deadline) {
        this.state = state;
        this.deadline = deadline;
    }

    public CircuitBreakerState getState() {
        return state;
    }

    public Long getDeadline() {
        return deadline;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(state.ordinal());
        out.writeBoolean(deadline != null);
        if( deadline != null ) {
            out.writeLong(deadline);
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        state = CircuitBreakerState.values()[in.readInt()];
        if( in.readBoolean() ) {
            deadline = in.readLong();
        }
    }
}
