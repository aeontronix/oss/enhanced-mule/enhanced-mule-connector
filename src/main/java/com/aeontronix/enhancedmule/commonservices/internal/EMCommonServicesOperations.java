package com.aeontronix.enhancedmule.commonservices.internal;

import com.aeontronix.commons.ReflectionUtils;
import com.aeontronix.enhancedmule.commonservices.api.CircuitBreakerState;
import com.aeontronix.enhancedmule.commonservices.internal.circuitbreaker.CBClosedRoute;
import com.aeontronix.enhancedmule.commonservices.internal.circuitbreaker.CBOpenRoute;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.core.api.el.ExpressionManager;
import org.mule.runtime.core.api.event.CoreEvent;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.extension.api.runtime.process.RouterCompletionCallback;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.slf4j.LoggerFactory.getLogger;


/**
 * This class is a container for operations, every public method in this class will be taken as an extension operation.
 */
public class EMCommonServicesOperations {
    private static final Logger logger = getLogger(EMCommonServicesOperations.class);
    @Inject
    private ExpressionManager expressionManager;

    @SuppressWarnings("unchecked")
    public void circuitBreaker(@Optional(defaultValue = "default") String circuitBreakerId,
                               @Optional(defaultValue = "PT10M") @Summary("Duration (in ISO8601 format) in which the error ratio will be calculated against") String errorThresholdPeriod,
                               @Optional(defaultValue = "100") @Summary("What percentage (0-100) ratio of error will cause the circuit breaker to be triggered") BigDecimal errorThreshold,
                               @Optional(defaultValue = "PT30M") @Summary("Duration (in ISO8601 format) until a closed circuit breaker switches to half-open") String closedDuration,
                               @Optional() @Summary("Dataweave expression that will be used to evaluate if an error has occurred (if set must return true or false)") ParameterResolver<Object> errorEvalExpr,
                               CBOpenRoute open,
                               CBClosedRoute closed,
                               RouterCompletionCallback callback) {
        final CircuitBreaker circuitBreaker = new CircuitBreaker(circuitBreakerId, errorThresholdPeriod, errorThreshold, closedDuration);
        final CircuitBreakerState state = circuitBreaker.getState();
        switch (state) {
            case OPEN:
                open.getChain().process(result -> {
                    if (errorDetected(errorEvalExpr, result)) {
                        circuitBreaker.handleErrorInOpenState();
                    }
                    callback.success(result);
                }, (error, previous) -> {
                    circuitBreaker.handleErrorInOpenState();
                    callback.error(error);
                });
                break;
            case OPEN_WITH_ERROR:
                open.getChain().process(result -> {
                    if (errorDetected(errorEvalExpr, result)) {
                        circuitBreaker.incErrors();
                    } else {
                        circuitBreaker.incOperations();
                    }
                    callback.success(result);
                }, (error, previous) -> {
                    circuitBreaker.incErrors();
                    callback.error(error);
                });
                break;
            case CLOSED:
                closed.getChain().process(callback::success, (error, previous) -> callback.error(error));
                break;
            case HALF_OPEN:
                open.getChain().process(result -> {
                    if (errorDetected(errorEvalExpr, result)) {
                        failHalfOpen(circuitBreakerId, circuitBreaker);
                    } else {
                        logger.warn("Successful operation while in half-open circuit breaker state, re-opening circuit: {}", circuitBreakerId);
                        circuitBreaker.openCircuit();
                    }
                    callback.success(result);
                }, (error, previous) -> {
                    failHalfOpen(circuitBreakerId, circuitBreaker);
                    callback.error(error);
                });
                break;
        }
    }

    private void failHalfOpen(String circuitBreakerId, CircuitBreaker circuitBreaker) {
        logger.debug("Failed operation while in half-open circuit breaker state, re-closing circuit: {}", circuitBreakerId);
        circuitBreaker.closeCircuit();
    }

    private boolean errorDetected(ParameterResolver<Object> errorEvalExpr, Result result) {
        try {
            if (errorEvalExpr != null && errorEvalExpr.getExpression().isPresent()) {
                final String expression = errorEvalExpr.getExpression().get();
                if (errorEvalExpr.getClass().getName().toLowerCase().contains("expression")) {
                    final CoreEvent event = (CoreEvent) ReflectionUtils.get(result, "event");
                    final TypedValue exprRes = expressionManager.evaluate(expression, event);
                    return isTrue(exprRes.getValue());
                } else {
                    return isTrue(expression);
                }
            }
        } catch (Exception e) {
            logger.warn("Unable to evaluate error expression: " + e.getMessage(), e);
        }
        return false;
    }

    private boolean isTrue(Object obj) {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        } else if (obj != null) {
            return obj.toString().equalsIgnoreCase("true");
        } else {
            return false;
        }
    }
}
